// PractCPP.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

//Declaracion de estructura nodo
struct nodo
{
	int num = 0;
	int edad = 0;
	string nombre;
	string apellido;
	nodo* sgte = NULL;
};
struct nodo* in, * an, * pfin;

//definir las funciones que usara el programa, sin implementarlas
void menu();
void insertar(int, int, string, string, bool);
void mostrar();
void buscar(int, bool);
void eliminar(int);
void modificar(int);
void pausa();
void imprimir_registro(struct nodo*, int);
//implementacion de funcion clrscr no disponible en vc++
void clrscr() {
	system("cls");
}
//mostrar una pausa en pantalla
void pausa()
{
	cout << endl << endl << "Presione [ENTER] para continuar...";
	system("PAUSE > NUL");
}
//insertar un nodo nuevo a la lista 
void insertar(int numero, int edad, string nombre, string apellido, bool al_inicio = false)
{
	nodo* nuevo = new(nodo);
	nuevo->num = numero;
	nuevo->edad = edad;
	nuevo->nombre = nombre;
	nuevo->apellido = apellido;
	nuevo->sgte = NULL;

	//si la lista esta vacia, insertar nuevo nodo al inicio
	if (in == NULL)
	{
		/*in = new(nodo);
		in->num = numero;
		pfin = in;*/
		in = nuevo;
		pfin = in;
	}
	//en caso de que ya hayan elementos, a�adir al inicio o final
	else if (al_inicio) {
		//Se selecciono insertar al inicio
		//Se enlaza el nuevo nodo con el inicio de la lista		
		nuevo->sgte = in;
		//Luego se convierte al nuevo nodo en el inicio de la lista
		in = nuevo;
	} else
	{
		//insertar nuevo nodo al final de la lista
		pfin->sgte = nuevo;
		//reemplazar el final de la lista con el nuevo nodo
		pfin = nuevo;
	}
	//hace que el puntero siguiente del ulitmo nodo sea un nodo vacio
	pfin->sgte = NULL;
}

void mostrar()
{
	int contar = 0;
	an = in;
	//si la lista esta vacia, alertar al usuario
	if (in == NULL)
	{
		cout << "\nLa lista esta vacia, inserte elementos para ejecutar esta opcion:)";
	}
	//recorrer la lista hasta que se encuentre el ultimo nodo
	while (an != NULL)
	{
		contar++;
		imprimir_registro(an, contar);
		an = an->sgte;
		cout << endl << endl << endl;
	}
	//hacer una pausa antes de mostrar el menu
	pausa();
}
//pasar un puntero e imprimir su informacion
void imprimir_registro(struct nodo* registro, int indice = -1) {
	cout << endl << "\t ";
	if (indice > 0) {
		cout << indice<<": ";
	}
	cout << "Codigo:" << an->num;
	cout << endl << "\t Edad:" << an->edad;
	cout << endl << "\t Apellido:" << an->apellido;
	cout << endl << "\t Nombre:" << an->nombre;
}
//buscar por valor un nodo en la lista en base al numero ingresado
void buscar(int numero, bool pausar = true)
{
	int flag = 0;
	an = in;
	//la lista esta vacia y no se puede buscar en una lista vacia
	if (in == NULL)
	{
		cout << "\n La lista esta vacia, inserte elementos para ejecutar esta opcion:)";
	}
	else
	{
		//recorrer la lista 
		while (an != NULL)
		{
			///comparar el nodo actual con el valor que se esta buscando
			//si se encuentra, mostrar en pantalla y encender bandera 'flag'
			if (numero == an->num)
			{
				
				cout << endl << endl << "\t Registro encontado:" << endl;
				imprimir_registro(an);
				flag = 1;
			}
			an = an->sgte;
		}
		//si la bandera nunca se encendio en el recorrido anterior, notificar al usuario que el valor no existe en la lista
		if (flag == 0)
		{
			cout << endl << "\n\t Valor no exite en la lista";
		}
	}
	if (pausar) {
		//pausa antes de volver al menu
		pausa();
	}
}

void modificar(int numero)
{
	int flag = 0;
	int numM, edad;
	string apellido, nombre;
	an = in;
	//la lista esta vacia, no se puede modificar un elemento en la lita porque no hay uno
	if (in == NULL)
	{
		cout << "\nLa lista esta vacia, inserte elementos para ejecutar esta opcion.";
	}
	else
	{
		while (an != NULL)
		{
			//se busca el nodo por el valor ingresado, si se encuentra 
			//se solicita al usuario ingresar un nuevo valor para reemplazar el valor anterior
			if (numero == an->num)
			{
				cout << "Antiguo Registro:"<<endl;
				imprimir_registro(an);
				//cout << "\n\n\t Ingrese Codigo: ";
				//cin >> numM;
				cout << "\n\t Ingrese Edad: ";
				cin >> edad;
				cout << "\n\t Ingrese Nombre: ";
				cin >> nombre;
				cout << "\n\t Ingrese Apellido: ";
				cin >> apellido;
				an->apellido = apellido;
				an->nombre = nombre;
				//an->num = numM;
				an->edad = edad;
				cout << "Registro Modificado:" << endl;
				imprimir_registro(an);
				//se enciende la bandera flag porque si se encontro y modifico el nodo
				flag = 1;
			}
			an = an->sgte;
		}
		//no se encontro el nodo, notificar al usuario
		if (flag == 0)   			//PROF said: bandera igual a 0 en caso no se encontrara el valor buscado
		{
			cout << endl << "\n\t El Codigo no fue encontrado";
		}
	}
}

//eliminar un nodo de la lista
void eliminar(int numero)
{
	char seguro[10];
	struct nodo* pant = NULL;
	bool continuar = true;
	//lista vacia, salir
	if (in == NULL)
	{
		cout << "\n\t Lista vacia";
	}
	else
	{

		//buscar en el primer nodo si esta el dato que se quiere eliminar
		if (in->num == numero)
		{
			//almacenar la referencia al nodo que se quiere eliminar
			an = in;
			//mover el enlace del nodo actual al nodo siguiente porque vamos a eliminarlo :D
			in = in->sgte;
			//cout << "\n\t Codigo eliminado: " << an->num;
		}
		else
		{
			//ya que el primer nodo no tenia el valor que se quiere eliminar, se recorre la lista
			an = in->sgte;
			pant = in;
			while (an != NULL && an->num != numero)
			{
				//almacenar el nodo que se quiere eliminar, si se encuentra, se sale del bucle
				pant = an;
				an = an->sgte;
			}
			//si el nodo no es el final de la lista, se elimina
			if (an != NULL)
			{
				pant->sgte = an->sgte;
				//cout << "\n\t Codigo eliminado: " << an->num;
			}
		}
		if (an != NULL) {
			imprimir_registro(an, -99);
			cout << endl << endl << "Esta seguro de eliminar? s/n" << endl;
			cin >> seguro;
			if (strcmp(seguro, "s") == 0 || strcmp(seguro, "S") == 0 || strcmp(seguro, "Y") == 0 || strcmp(seguro, "si") == 0 || strcmp(seguro, "y") == 0) {
				continuar = true;
			}
			else {
				continuar = false;
			}
			if (continuar == false) {
				cout << endl << "No se elimino.";
				return;
			}
			//como se guardo la referencia al nodo encontrado, se procede a eliminarlo
			delete(an);
		}
		

	}
}

void menu() //menu de funciones
{
	//�cambiar el color a verdecito!
	system("color a");
	int opc, numero, edad;
	string apellido, nombre;
	bool al_inicio = true;
	int opt_eliminar = 0;
	//mostrar el menu!!!
	do
	{
		cout << endl<< endl << endl << "---------------------" << endl ;
		cout << " Menu de opciones \n";
		cout << "--------------------- \n";
		cout << "\n 1. Insertar al inicio";
		cout << "\n 2. Insertar al final";
		cout << "\n 3. Mostrar";
		cout << "\n 4. Buscar";
		cout << "\n 5. Modificar";
		cout << "\n 6. Eliminar";
		cout << "\n 7. Salir";
		cout << "\n\n\t Ingrese su opcion: ";

		cin >> opc;
		clrscr();

		switch (opc)
		{
			//insercion a la lista
		case 1:
		case 2:
			
			if (opc == 2) {
				al_inicio = false;
			}
			cout << "\n\t Ingrese Codigo: ";
			cin >> numero;
			cout << "\n\t Ingrese Edad: ";
			cin >> edad;
			cout << "\n\t Ingrese Nombre: ";
			cin >> nombre;
			cout << "\n\t Ingrese Apellido: ";
			cin >> apellido;
			cout << "\n\t Apellido: " << apellido << endl;

			insertar(numero, edad, nombre, apellido, al_inicio);
			clrscr();
			break;
			//recorrer y mostrar la lista
		case 3:
			mostrar();
			clrscr();
			break;
			//buscar un valor en la lista
		case 4:
			cout << "\n\t Ingrese Codigo a buscar: ";
			cin >> numero;
			buscar(numero);
			clrscr();
			break;
			//cambiar el valor de un nodo
		case 5:
			cout << "\n\t Ingrese Codigo a sustituir: ";
			cin >> numero;
			modificar(numero);
			pausa();
			clrscr();
			break;
			//eliminar un nodo de la lista
		case 6:
			cout << "\n\t Ingrese Codigo a eliminar: ";
			cin >> numero;
			eliminar(numero);
			pausa();
			clrscr();
			break;
			//no hacer nada con la lista y devolver el control a la instruccion main!!! :D :D :D 
		default:
			opc = 7;
			cout << "Salida del programa\n";
			break;
		}
	} while (opc != 7); //la opcion siete sale del programa, cualquier otra repite el menu y sus acciones
}

int main()
{
	//mostrar el menu!!! :D :D ^^ ^^ ^^
	menu();
	return 0;
}

